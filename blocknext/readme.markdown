# BlockNext

BlockNext is a central hub of interfaces for MicroWebApi interactions with 
URA/PTI-Compliant systems. Generally, all URA/PTI-Compliant should pass the 
following Data Security Protocols:

1. HIPPA
2. PCI
3. AML/KYC

## Overview
j
The given system catalyzes three unitary methodologies to dynamically construct 
identity compute statements throughout the evolution of the network. Statements 
are non-trivial abbreviated reductions of total system state.

## Authentication Substrate

Authentication ultimately must be a human meaningful exercise, but one which 
requires zero-work. This requirement makes the given subsystem obliged to 
adopt weightless key tooling for the management and configuration of keys. 
Keyservers drive the Rule of Recognition of the system to derive co-evolutary 
smooth digital transitional states of identity over time.

## Authorization Substrate

The given subsystem is a metastructural manager of action spaces, not merely 
the actions themselves but rather the domains in which actions occur. Action 
results resolve within smooth digital transitional states of identity through 
immanent planes of existence which are constrained as boolean satisifiable 
vector-anomalies within polynomial time bound-error models. Actions of a most 
general unitary reference architecture are procedurally yet non-linearly 
situated within a fundamental situation such that microauthorization (true idea) 
coheres with its object (satisfaction of N-clauses as physical assets of the 
world).

## Amortization Substrate

A generic blockchain engine is described and implemented with the co-located and 
geo-fenced virtual space of denominated actions with a transmedia-hypermedia fully 
integrated ontology.
