/**(
 * @fileOverview ./blocknext/actions/CreateAction.js
 * @description
 * The CreateAction pertains to the resolution of contracts which bear provenance relations
 *  which might be attached to SPOOL-like transactions.
 * )
 */
