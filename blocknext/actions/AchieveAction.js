
/*(# Achieve Action
@fileOverview ./blocknext/actions/AchieveAction.js
@author nerdfiles <hello@nerdfiles.net>
#)
 */

(function() {
  var ArchiveAction;

  ArchiveAction = function(app) {
    var route;
    route = function(req, res, next) {

      /*
      @inner
       */
      var challenge;
      return challenge = req.challenge || {};
    };
    return app.all('someRoute', route);
  };

  module.exports = ArchiveAction;

}).call(this);
