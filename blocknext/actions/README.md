# Actions

Starting from https://schema.org/Action?s. Actions are a special case of models.

Most CRUD apps only require a few set of actions to describe file read, upload, 
transfer, counter, etc. behaviors (counters are actually part of equally basic 
InteractionCounter; so some simplified app models are pretty clearly represented 
in this future-forward strategy, particularly under the UpdateAction). We can 
add precision to actions like WriteAction where more human-centric design 
patterns create or prompt such expectation, even if they are metonymically or 
homonomic, or converge within semantic fields that are polynomially bound.
