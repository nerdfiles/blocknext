# Entities

Starting from https://schema.org/Intangible?s. StructuredValues are a special case of models. 
Most CRUD apps can be built based solely on models. Entities adds a basic level of abstraction 
which can support Distributed Resource-oriented Applications with Contracts (Actions, also 
considered a basic level application layer; Contracts are Entities with Action).
