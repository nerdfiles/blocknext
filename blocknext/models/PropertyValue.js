/**(
 * @fileOverview ./blocknext/models/PropertyValue.js
 * @description
 * PropertyValue is a model that enables a majority of first-class schema.org
 * attributes to be implemented as microdata.
 * @usage
 * <!-- Product: Property ID for clarifying the meaning of a property: URI from external vocabulary -->
 * <div itemscope itemtype="http://schema.org/Car">
 *   <img itemprop="image" src="station_waggon123.jpg" />
 *   <span itemprop="name">Station Waggon 123</span>
 *   <div itemprop="additionalProperty" itemscope itemtype="http://schema.org/PropertyValue">
 *       <span itemprop="name">Luggage Capacity (seats folded)</span>:
 *       <span itemprop="value">500</span>
 *       <meta itemprop="unitCode" content="LTR">liter
 *       <link itemprop="propertyID" href="http://purl.org/vvo/ns#luggageCapacitySeatsFolded" />
 *   </div>
 * </div>
 * )
 */

var mongoose = require('mongoose');
var Schema = mongoose.Schema;
var PropertyValue = new Schema({
})
  .statics('getPriceByTrx', function (callback) {
    var self = this;
    self.find({trxId: self.trxId }, callback);
    return;
});

var propertyValue = mongoose.model("PropertyValue", PropertyValue);

module.exports = {
  propertyValue: propertyValue
};
