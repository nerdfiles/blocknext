# Models as Intangibles

Starting from https://schema.org/StructuredValue. Most apps don’t need actions, 
or entities. They’re “TODOapps”. Distributed Resource-oriented Applications cover 
this class of app-drudgery quite trivially. Plug in Actions and Entities where 
you see fit, but they should decorate or extend actions inherited to more basic 
models. Models are like interfaces of modal action; and the API is intended to 
map capabilities assumed by the browser to features of models which are dressed 
according to the specificity of the route contract.
