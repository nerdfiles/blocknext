/**(
 * @fileOverview ./blocknext/models/OwnershipInfo.js
 * @description
 * A likely candidate for for SPOOL decorators to work with specialized trade
 * and transfer transactions, as opposed to automated transaction.
 * )
 */

var mongoose = require('mongoose');
var Schema = mongoose.Schema;
var OwnershipInfo = new Schema({
    name       : {
      type     : String,
      required : true
    }
  , published  : {
      type     : Date
  }
  , created    : {
      type     : Date,
      required : true
  }
}).

  statics('getOwnerByAlias', function (callback) {
    var self = this;
    self.find({name: self.alias}, callback);
    return;
}).

  statics('getOwnerByName', function (callback) {
    var self = this;
    self.find({name: self.name}, callback);
    return;
});

var ownershipInfo = mongoose.model("OwnershipInfo", OwnershipInfo);

module.exports = {
  ownershipInfo: ownershipInfo
};
