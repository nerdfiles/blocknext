
/*(# Contact Point
@fileOverview ./blocknext/models/ContactPoint.js
@author nerdfiles <hello@nerdfiles.net>
#)
 */

(function() {
  var ContactPoint;

  ContactPoint = function(app) {
    var route;
    route = function(req, res, next) {

      /*
      @inner
       */
      var challenge;
      return challenge = req.challenge || {};
    };
    return app.all('someRoute', route);
  };

  module.exports = ContactPoint;

}).call(this);
