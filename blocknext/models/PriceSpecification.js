/**(
 * @fileOverview ./blocknext/models/PriceSpecification.js
 * @description
 * PriceSpecification is a mini_charge plugin primitive use to modularize
 * Stripe, SPV, Paypal, etc., network services.
 * )
 */

var mongoose = require('mongoose');
var Schema = mongoose.Schema;
var PriceSpecification = new Schema({
})
  .statics('getPriceByTrx', function (callback) {
    var self = this;
    self.find({trxId: self.trxId }, callback);
    return;
});

var priceSpecification = mongoose.model("PriceSpecification", PriceSpecification);

module.exports = {
  priceSpecification: priceSpecification
};
