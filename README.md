# Blocknext: Next generation banking.

<div style="text-align: center;">
<a href="http://blocknext.com">
<img alt="BlockNext" src="https://gitlab.com/makers-consulting/blocknext/raw/master/prototype.png" />
</a>
</div>

Block Next is the future of Internet Finance.

We are built on the internet of value, and provide a suite of financial services 
that meet 90% of the world's banking needs.

## Problem

Half the world is unbanked. How do we bring inclusivity to the global economy 
to promote fair exchange? Blocknext turns the pyramid of power, bringing universal 
access to the global financial system.

## Setup

Run `grunt setup` to install dependencies for the front end.

## Build & development

Run `grunt` for building and `grunt devel` for development

## Serving Locally

Run the local server with `node server/www.js`.

## Style Compilation

Move into the `app/styles/` folder and run `sass --watch sass:css`

## Testing

Running `grunt test` will run the unit tests with karma.

## Management

### “Building” content from static site copy

Content Authors (CA) may use http://prose.io to update the `contents` directory.

This project uses [Punch](https://github.com/laktek/punch/wiki) to convert 
Moustache templates into static site pages that essentially map public URLs 
to files created in the repository. Github, effectively, and logical naming of,
files becomes the CMS. This should happen during the Build phase (Grunt).

## Sitemap

1. Home/Search  
   (Educate, Start Search): Use Mixpanel tracking in AngularJS to track hover, keydown, mousedown, scroll, visibility of real estate within viewport bounds. So virtual data is “watching all customers” to collect info on them.
2. Chat  
   (Simple) automated chat with conversational UI as “sequences of questions user would choose on other parts of the site” but in “text flow” with momentary data (relative to other data) inquiry-response.
3. Login  
   A login screen that stores username optionally.
4. Account (Deals, etc.)  
   Following Ontology: Account screen shows “following ontology” view with lists, dashboards, etc. from timeseries graphs as the real estate logs certain aggregate/macro events from Mixpanel 
   Reactive Testimonials: First “callout lead paragraph” of the page implements [Tangle](http://worrydream.com/Tangle/) to produce reactive testimonial look-and-feel. So unfortunately I don’t have 
   a ready place to design the testimonials and fit them as content — but I think the reactive “account” overview would be clean solution to showing value in an interactive, 
   projective heuristic. The text summary becomes an interactive “textualization” (visualization) of ROI with a blob of text. Testmonials are like “templates” we inherit 
   to demonstrate the “flow of the conversation” perhaps in the lender’s own language/vernacular. So we could do natural language processing to identify common language 
   terms to make hookable in tangle. Must be quantitatively/statistically enforcing terms.

## Map Tool

LoanCoin (P, C) pairings are displayed on the map at LatLngs. Monthly 
payments from investor updates a counter for the map marker that represents P.

1. Aged markers  
   Newer pairings will appear hotter until the "flame" fizzles out.
2. Annotated markers  
   Some Loans have notes. Render these in a bottom sheet for markers.
3. Payment Conformance markers  
   You can see the property with a countdown of payments. Age from start  
   of Loan. Assertion of Payment Performance which may extend behind age.  
   Age is a constant that becomes less visible as successive payments are 
   met.
4. Profit Positive markers  
   Investors who have extended their payment obligations will augment  
   markers with an icon that represents earnings beyond initial  
   loan offering differential of total interest with new total interest.
5. Multisig markers  
   Shared Loan: A marker may have two investors with one Loan. If the users opt-in  
   for the Loan, the marker appears. One signed investor will determine  
   the marker "inactive" or "pending".  
   Flagging: A marker may receive reports from all parties on the validity of the  
   real estate.
6. Chained markers  
   Lender Ownership Influence: Lender has multiple loans that creates a chained network. If same LatLng, Spiderify/Cluster.  
   Investor Chained Ownership: One investor has multiple loans for multiple estates.
7. Voting Sentiments on markers  
   Investors will "like" a property with a certain mood description.
8. Business Card markers  
   Investors can attach business cards to a marker if the pairing exists.
9. Activity markers  
   Real estate may appear without a pairing but can be viewed by all. Pairings  
   only appear to investors who are logged in.  
   Activity depends on engagement model (clicks, views, etc.) of real estate  
   Web views.
10. Chatty markers  
    Public markers instantiate chatrooms.
11. Published markers  
    Investors may publish their successful or unsuccessful pairings.
12. Invitation markers  
    Investors may invite other users to view their pairings. Or invites to Request for Loan.
13. Kiosk Updates / Kiosk Context Menus  
    Accessing limited functionality and info (history) of managed Web kiosk. Kiosk is access 
    point to financial services which have hitherto been available only to high end customers 
    of banked persons.
14. Digital Notary  
    Service to document assets. Assets stored under map marker.

—
App starts from personal boilerplate available here: https://gist.github.com/nerdfiles/65d1242cee4ad5b478f3
