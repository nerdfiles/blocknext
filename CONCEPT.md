# Block Next

Block Next is the future of Internet Finance.  We are built on the internet of 
value, and provide a suite of financial services that meet 90% of the WORLDS 
banking needs.  Product Lines:

## MVP

1. *Bank in a Box*: Federated Hardware provides an access portal to the global  
   economy. Block-Next provides tools that meet the needs of anyone who wishes  
   to be a part of the global economy.
2. *Asset Security Systems*:  Do you have a title document? Do you have claim to  
   assets that are rightfully yours?  Block-next is your portal to tools that  
   securely converts real-world assets into unique digital assets.  Prove the  
   existence and ownership of what is yours.
3. *Fraud Mitigation*:  By using Distributed Hash Table Architecture,  
   Block-Next’s security systems allow traceability and auditability of  
   transactions that occur, without fear of evidence tampering or questioning  
   the fidelity of the transaction.

## Mission

Block Next’s goal is to bring the polarized global economy back to a level 
playing field, where everyone with a finger is capable of being included as 
a valued member of the global economy.

Our systems form the backbone of the Next global economy, where individuals are 
empowered and enabled to join the developed economy from the developing worlds.

Through Block Next, The Bottom of the Pyramid will no longer be neglected.  They 
will receive fair trade, fair exchange, access to the goods, services, and means
that have been out-of-reach because of the way the cards have been stacked.

## Coinalysis

### NXT Specifications

    Cryptocurrency with proof-of-stake (POS) forging
    Total coins            : 1,000,000,000 — NXT Each NXT is divisible to 10^8 NQT
    Hash Algorithm         : SHA-256
    Encryption Algorithm   : Curve25519
    Block speed            : 1 Minute
    Transactions per block : 256
    Max attachment size    : 42K — POS difficulty is recalculated every two block
    POS reward             : only transaction fees

