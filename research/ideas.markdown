# Ideas

## Listening for hierarchical events

```js
    h.on(['v1/interface', 'services', 'bank'], function () {
        console.log('v2/interface services encoding');
    });

    h.on(['v1/interface', 'services', 'map'], function (msg) {
        console.log(msg, ' services map');
    });
```

## Winograd

## QM

## Executing a hierarchical hashlock

```js
    h.emit([
        ‘/p2pkh/abcde/’, 
        '/asset/can_missive/', 
        '*'
      ], 
      'v1/interface'
    );
```
