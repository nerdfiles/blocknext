% Existential Debriefing
% Aaron Alexander
% May 5, 2016

# TOC 

* Overview
* Universe
* Project Universe
* Social: Problem
- Tribal Tippers and Pragmatic Tippers
- Robustness Principle
* Social: Distributed resource-oriented applications
* Banking: Problem
- r > g
- fractional reserve banking and quantitative easing;
- but NAFTA and scale, fairtrade
* Banking: Apps
* Industry: Problem
- WebApps suck (not to mention the proliferation of idle competition)
- Healthcare sucks
* Industry: Apps
* Technology: Problem
- It's just a digitally signed database.
- Blocksize and Bottlenecks: Object Databases for Redundancy and Parity Bits
- Technology for Trustlessness and Censorship Resistance: Encrypted Corrupt Policies? (If we're saying politics is corrupt, but we have to "compromise" through legislation and dealing that is inherently corrupt. Consumers benefit how?)
- "You can't have Open Blockchains and a welfare state."
* Technology: Apps
- Distributed Teaching Assistants
* Closing

# Overview

* Problem
* Project
* Social
* Banking
* Industry
* Blockchain

# Problem

<img src="images/cern.jpeg" />

# Project

<img src="images/BlockNext-Project.png" />

# Social

Social life is made up of People who Attribut(e|al) within Social Attribution 
(Behaviors) and Social Functional Phenomenology (Epistemology) expanding 
Amorous, Legal, Psychological, Thought As Decentralized Virtual Embodied Local 
Datum made of Particle(s) from Postulate(s) exposing an Amorous Absolute Null.

# Banking

Corporations (Relational) Aesthetical Context Banking Econometrics Economical 
Physical Material Extension As Centralized Virtual Embodied Nonlocal Object 
Thing Referent Aesthetic Objective One

# Industry

Content (Attitudinal) Political Narration Industry Cybernetics Ethical Moral 
Cultural Thought As If Decentralized Distributed Artificial Local Value Idea 
Proposition Politic Subjective Multiple

# Blockchain

Machines (Extensional) Biological Value Blockchain Infocology Chemical 
Environmental Physiological Extension As If Centralized Distributed Artificial 
Nonlocal Energy Wave Measurement Biologic Relative Infinite

# Closing

A compilation of r > g
