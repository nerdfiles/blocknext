% Periodic Table of Information
% Aaron Alexander, Scott Akers
% June 15, 2016



# Topics

1. Philosophy : Why Philosophy, Why Now?
2. Identity   : Cybernetic Idency
3. Industry   : Infocologically-sane Industries
4. Society    : Bounded Rationality, Boolean Satisfiable Equilibriums: Conjectures and Refutations Model of Social Normativity
5. Banking    : The New Measurement Paradox and Fiduciary Instrumentation
6. Technology : Blockchains and Distributed Resource-oriented Architecture
7. Questions  : Inquiry? Comments?

# Philosophy: Why Philosophy, Why Now?

just proving transitive trust is bad for trust

# Identity: Cybernetic Idency

# Industry: Infocologically-sane Industries

# Society: Bounded Rationality, Boolean Satisfiable Equilibriums: Conjectures and Refutations Model of Social Normativity

# Banking: The New Measurement Paradox and Fiduciary Instrumentation

# Technology : Blockchains and Distributed Resource-oriented Architecture

# Questions  : Inquiry? Comments?

# Deprecated: Basic issue of privacy

    “Since everything lies open to view there is nothing to explain. For what
    is hidden, for example, is of no interest to us” (no.126).
    — Ludwig Wittgenstein

L.W. was talking about zero knowledge systems of private transactions: “transaction at a distance”.

The private language argument is a channel coding problem in disguise. Funny, that!


# Basic issue of privacy (redux)

    With democracy stalled at the Web access starting gate, our ability to assert
    dissention is regulated by the folks who make the rules. Rules we don't
    really even know about. There is a fundamental flaw in the basic reference
    architecture that defines the Internet system itself. It's built on an
    inadvertent misapplication of transaction identity rules to people identity
    requirements. This is bad business practice and about to get worse.
    — [Model of Everything](http://www.google.com/patents/US7774388).
    — Margaret Runchey. Late 2007.


# Double Spend Attacks Happen All The Time

http://respends.thinlink.com/

We just drop them silently.


# Getting Started

- TCP for the Web?
  - [STOMP](http://jmesnil.net/stomp-websocket/doc/)
  - [DualApi](https://github.com/plediii/dualapi)

Establishing a simple protocol to support a logical layer for REQ-TCP simulation within REST to
align and shape hardware drivers at a lower layer.


# Metaphor Today: Smalltalk and Cyberspace

    Smalltalk's design--and existence--is due to the insight that everything we
    can describe can be represented by the recursive composition of a single kind
    of behavioral building block that hides its combination of state and process
    inside itself and can be dealt with only through the exchange of messages.
    — smalltalk: philosophy, metaphor, semantics, syntax
    — http://billkerr2.blogspot.com/2007/09/smalltalk-philosophy-metaphor-semantics.html

# Opentalk

- Smalltalk Selectors: Message selectors (such as +) are called selectors because they select properties from the receiving object.
- Smalltalk Cells : Biological cell, whose contents of the cell are protected or encapsulated and that they respond to messages from outside.
- Smalltalk Monads: unary, binary and keyword: That selects the part of my mind that thinks about directions.

https://docs.openchain.org/en/latest/api/ledger.html

# Privacy and Meaning

<blockquote>
<p>MILLER: "Let me appeal as you did to the Blue River. Suppose I take a visitor to the stretch of river by the old Mill, and then drive him toward Manhatten. After an hour-or-so drive we see another stretch of river, and I say, “That’s the same river we saw this morning.” As you pointed out yesterday, I don’t thereby imply that the very same molecules of water are seen both times. And the places are different, perhaps a hundred miles apart. And the shape and color and level of pollution might all be different. What do I see later in the day that is identical with what I saw earlier in the day?"
<p>WEIROB “Nothing except the river itself."
<p>MILLER: “Exactly. But now notice that what I see, strictly speaking, is not the whole river but only a part of it. I see different parts of the same river at the two different times. So really, if we restrict ourselves to what I literally see, I do not judge identity at all, but something else."
<div>— A Dialogue on Personal Identity and Immortality. Perry, John.</div>
</blockquote>

An example of "mereology" (study of part-whole relationships) being used in a practical argument to get under an atheist's skin.


# Periodic Table of Information + Canonical Record System

    U 0. ALL EVENTS - EVERYTHING
    R 1.0. EVENT ASSERTIONS                 1.1. EVENT OBJECTS
    A 1.0.0. PRINCIPLE  1.0.1. TRANSACTION  1.1.0. PERSON   1.1.1. PROPERTY

A syntax to express aggregation, explication, etc. for cyberspace.

What is *in* cyberspace? More importantly: How does cyberspace hang together?

# Identity

    Ontology      Mode             Metastructure   Domain         Sector        Knowledge
    People        (Attributal)     Social          Attribution    Social        Functional Phenomenology (Epistemology)   Amorous       Legal           Psychological   Thought As        Decentralized   Virtual       Embodied    Local      Datum    Particle    Postulate     Amorous     Absolute      Null
    Corporations  (Relational)     Aesthetical     Context        Banking       Econometrics                              Economical    Physical        Material        Extension As      Centralized     Virtual       Embodied    Nonlocal   Object   Thing       Referent      Aesthetic   Objective     One
    Content       (Attitudinal)    Political       Narration      Industry      Cybernetics                               Ethical       Moral           Cultural        Thought As If     Decentralized   Distributed   Artificial  Local      Value    Idea        Proposition   Politic     Subjective    Multiple
    Machines      (Extensional)    Biological      Value          Blockchain    Infocology                                Chemical      Environmental   Physiological   Extension As If   Centralized     Distributed   Artificial  Nonlocal   Energy   Wave        Measurement   Biologic    Relative      Infinite

Too Spinozistic? *Probably*. To put it another way:

![Futurist Heather Schlegel's narratological metastructure of heterogeneous transactions.](http://www.heathervescent.com/.a/6a00d83452604669e2015435c41e15970c-pi)


# People: From Justice

- StoryBook Generators (Narratology)
- Minimalistic School (Computational Linguistics)
- HLA Hart (Retributive Justice, Rule of Recognition)
- Telepathy as Folk Identity
    We have misapplied telepathy: why should the amorous mode of reality be
    grammaticalized by corporate identity as economic performance or static theory
    of efficiency about law or aged science of messages themselves? Telepathy
    is the aggregate shift of musical, folk identity — who are ‘we’ for any Rule of
    Recognition?

![narratologicallevels](images/narrlevels.png)


# Corporations: From Fairness

- Rule of Recognition
- Fairtrade
- NAFTA
- [EDGAR](https://www.sec.gov/edgar/searchedgar/companysearch.html)
- DUNS Number
- Banks
- Federal
- Factom

## Oops

- EDI/SAP/Google Health tragedy of the commons

Interoperable? Continuous? Federated?

![The governance of fair trade system: evidence from small honey producers in Rio Grande do Sul](images/ft-transaction.gif)


# Content: From Provenance

- P2P Platforms
- The Web
- W3C
- Bittorent
- JS (Stripe, jQuery, CDN, etc.)
- Ascribe ([SPOOL](https://github.com/ascribe/spool))
- ChangeTip
- Telecommunications

## Opps

- bitcongress

Decentralized? Distributed? Authorized?

![cdd](https://upload.wikimedia.org/wikipedia/en/b/ba/Centralised-decentralised-distributed.png)


# Machines: From Rule

- CRISPR
- Transaction Continuums: Bitcoin, Ethereum, etc.; Transactions open to View (privacy)
- See [Blockchains wiki](https://gitlab.com/nerdfiles/knowledge-base/blob/master/Blockchains.wiki).

## Opps

- Permissions Debate
- Blocksize Debate
- Web Accessibility
- SEO and SPAs
- JSONAPI
- Publishing — yuck! Regulatory nightmares. Fees. Authoritative?
- Banks have the same problems as Academics — static theory over dynamic living cybernetic reality.

![dnachain](http://faculty.scf.edu/keirlem/BSC_1007_eText_UNIT_2/TEXT_Biological_Macromolecules/DNA-RNA%20model.fw.png)
![Identifying functional links between genes by evolutionary transcriptomics ](http://pubs.rsc.org/services/images/RSCpubs.ePlatform.Service.FreeContent.ImageService.svc/ImageService/Articleimage/2012/MB/c2mb25054c/c2mb25054c-f5.gif)


# Simple Revisions

- Extend CreativeWork (Content), Organization (Corporation), and (Machine) with Actions to the full extent that People are treated.
- Add http://schema.org/Machine
  - People/Machines/Corporations/Content are/can performs actions when Roles (longterm)/Activities (shortterm) are considered: Buyable, Donatable, Orderable, Payable, Quotable, Sellable, Tippable...

# Mathematics is ontology

Establishing mathematical models based on negative cycle detection:

    Notice    TRX
    Related   TRX
    Continue  TRX
    Organize  HVM
    Order     HVM
    Rearrange HVM

Abstract, generalize Nakamoto/Piketty to establish a mathematical definition of Respect:

    Compute identity statements for entity sets on weighted acyclic graphs.

This assert of Respect is applicable to Machines, Corporations, People, Content. The model
itself above is a minimalistic framework for discovery of respect relations. Applying
a algebraic matrix of identity statements as the total aggregation of URA atoms (a
notion of factivity, or the Order of Factivity).

We might also call this a Mathematical Definition of HLA Hart’s Rule of Recognition.
Learn more about the [discretion thesis](http://www.iep.utm.edu/legalpos/#H3).

Judging such equivalence, we might arrive at a demonstration such that

    t = dv^2

    transaction_event = data * value squared


# More Platforms, More Problems

    * Miner centralization
    * Infinitude of applications, all of which require similar ID reqs
    ** Tokenized Action Spaces
    ** “E-mail and password”
    * Bank in a Box
    ** Most apps are
    ** Ontological formaulations or reformulations of identity data (Explication)
    ** User interfaces or interaction methods given the medium they are accessed through (bounded explication).
    * Deploy
    ** Apps are aesthetically congruent methods of communication
    ** Determined by the user’s wants, needs, desires of
    ** inbound or outbound informatioin

![bob](images/bank-in-a-box-product-shot-1.jpg)


# Component Level Digital Signing

* Code Signed Continuous Build Processes
* E2E Encryption (JScrambler, seifnode, etc.)
* Sandboxed Personas
* Machine-to-Machine Identity
** Surrogate Identity
** Metanomic Identity
** Hyperdata
* Content-to-Content Identity
** Automated Identity
** Identity encoded into Quantum Algorithms (no measurement approach)
** Stateless Quantum Events


# Logarithmic Category Systems

* Classifiers
* Bitcoin
* Software-defined Communications
** Perceptron Modeling of QLS/QLD interactions, logical event spaces, etc.
** Frequency-based Fault Tolerant Systems
* State Machine Structural Relations
** “Web is fundamentally a distributed hypermedia application.”
** “Web is the most hostile development environment imaginable.”
* Prediction Machines (Link Prediction, Transaction Prediction)


# Logarithmic Measurement

- Logarithmic Categories
** discovered based on predictions about Log Pairs
- Generalized Adjunction
  * to determine inductional structure (metastructure)
- Logarithmic Ramification
  * between Pairs to extend nonconstant, holomorphic maps (birational surface of hardware layer constraints)


# Constructivistic Advantage

- Logarithmic Metastructure Systems: Building network interfaces from generated log pairs.
- Logarithmic Block Universe based on Category Equalisation


# Existing Ideas

- IPFS
- “Infocology”
- W3C
- MaidSafe, Storj, “Bitcoin”, Dogecoin, CryptoNote, Darkcoin, Monero,
  Potcoin, Satoshi Dice, ChangeTip, -- lots of “metadata”.

Who creates the Standard cannot be allowed to assume the hyperparameters of
Identity (metastructures).

Applies boolean satisfiable ideal information channel model:

    γ(σ) = log(α − β)

Where else can this be applied?


# Natura Naturata as Logical Layer

    Transitive Metastructures      (Notice TRX)
    ‘Pataphysics of Metastructure  (Related TRX)
    Countable Metastructures       (Continue TRX)

- Transmedia Transaction Model
- Non-natural, Non-neutral Information
- Information - Meaning = Data


# Natura Naturatans as Logical Layer

    Ontological Plenitude of Metastructure  (Organize HVM)
    Modal Transfer of Metastructure         (Order HVM)
    Principle of Sufficient Reason          (Rearrange HVM)

- Hypermedia View Model
- No brute facts, “Quine’s Variable”
- Information = Data + Meaning


# Transitive Metastructures

Thinking: “[represent] the maximum possible equilibrium between belonging and inclusion ...
          [It] is the ontological schema for normality.

Extended: a probability amplification technique applied to amplify the assignment which
          maximizes the number of weighted clauses, encoded into quantum algorithm that
          treated transmitted states, not enduring calculation/measurement itself (
          cost of observation)


# Language and Memory with ‘Patadata

Metastructures are measured using the engine of ‘Pataphysics:

    Syzygy    (converse of axiom schema of epislon-separation)
    Clinamen  (converse of axiom of union)
    Antimony  (converse of axiom of pairing)
    Anomaly   (converse of axiom of empty set)
              ‘Difference which makes a difference.’
              Repressed part of the rule that ensures its exception.
              Interferent information.
    Absolute  (converse of axiom of induction)
    ‘Pataphor (converse of axiom of extensionality)
    ???       (converse of axiom schema of epislon-collection)


# Nondenumerable Metastructure

One as an operation on 4 categories of metadata:

    social        (ghost in the machine)
    aesthetical   (dadata)
    political     (power set through puns)
    biological    (singularities)


# Epistemic Problems

- Semantic Information is like Analytic Knowledge
- `{Concepts,Ideas}.reduce(GettierProblem)`


# Technological Problems

- ‘To what degree does the media-type support machine-to-machine interactions?’
- ‘Does the media-type work for more than one application protocol (HTTP, FTP, etc.)?’

- Enter *transmedia modeling*, then *degradable media models*.


# Knowledge as JTB, Information as MFN

    S is information iff
        [S is meaningful]
        [S is well-formed]
        [S is not null]

    S knows p iff
        [S believes p]
        [S has evidence for p]
        [p is true]

    S trusts that p iff
        [S authenticates p]
        [S authorizes p]
        [S amoritizes p]


# Information as Communicative Texture

    $ is information iff
        [S is well-formed]
        [S is not null]

Mathematical Theory of Communications: NP


# Information as Communicative Praxis

    $ is information iff
        [S is meaningful]
        [S is not null]

Logarithmic Metastructure: <abbr title="">BQP</abbr>


# Classical Information Model, CIM

    S is information iff
        [S is purposeful]
        [S is well-formed]
        [S is not null]

Properties:

- Low Entropy
- Reliable Metastructure
- Small Data Deficit
- Infocologically Conservative
- Cantor’s Paradise


# Minimalistic Information Model, MIM

    S is information iff
       [S is purposeful]
       [S is not null]

Properties:

- High Entropy
- Low Metastructure
- Large Data Deficit (Digital Dark Age?)
- Information Surplus
- Maxwell’s Demon


# Conceptual Sphere Mapping

    Belief ---------------- Justification ------------- Truth : Knowledge
    |                                                       |
    Human-meaningful        Decentralized              Secure : Zooko’s Triangle
    |                                                       |
    Identity                Accessibility        Availability : Information
    |                                                       |
    IAM                         SDQCP                  P2PFSM : Software-defined Realities
    |                                                       |
    AuthenToken              AuthorToken           AmortToken : Cybernetic Reality
    |                                                       |
    ID ------------------------- PTI -------------------- CRS : NextTech


- IAM: Web-ready Asset Management with Private Key Authentication as an Asset
- SDQCP: Software-defined Quantum Communication Protocol based on Log Pair encoding to quantum state event potential for shared measurement or side-channel stateless swap compute.
- P2PFSM: Distributed Finite State Machine with Stateless Value Generation

# Fault Tolerant Front Ends

- [ft-html](https://github.com/nerdfiles/ft-html)

# CRS (Historically inherits from Unitary Reference Architecture)

    Event
    Object
    Entity
      Purpose
    Transaction/Assertion
    List
      Table
        Database
          Program
            Computer
              Intra-inter-extra network
                Global network
                  Cyberspace
    Group Mnemonics / Metanomic Identity
    XYZT/gt

# Deflationary Orders of Trust for Distributed Cryptological Namespaces

Apply Kripkean Semantics to Cryptological Systems:

    Uniqueness                 $         /aka/$President                        Possibility                       ◇
    Persistence                _         /aka/_endurance                        Necessity                         □
    Universality               -         /ui/btn--payment or /ui/p-text         Names                             Millianism
    Scalability                .         /ui/WebPage.breadcrumb                 Conjunctive Inclusion             & or ^
    Evolvability               +         /semver/1.0.0+38293/packageName        Disjunctive Inclusion             + or v
    Opacity                    * (or **) /A/produces/A/ or /A/**/C              Arrow (Material Entailment)       ⊃ or >
    No Side-effects            '         /savedRecord' (H Factor: LT)           Negation                          -
    Global uniqueness          ()        /style/(idAttr)                        Universal Quantifier              () or ∀
    Optional sameness          (())      /style/((className))                   Existential Quantifier            ∃ (quantifier exchange?) or `P(query=None)` (hopefully our API can be this simple)
    Fully Non-dereferenceable  !         /document/!main                        definition                        ≔ or ≡

# Thereoms

     □(PK0→PKn) → (□PK0→□PKn)
    ~□(PK0→PKn) v (□PK0→□PKn)

    it is necessary that
      if productivity-based income by a wide margin outpaces
          over long periods of time average return on investment

     □PK0 → □□PK0
    ~□PK0 v □□PK0

     ◊PK0 → □◊PK0
    ~◊PK0 v □◊PK0

# System of Logic

    Propositional Logic
        Predicate Logic
            Natural Laws
                Mathematical Logic
                    (Metaphysical Logic|'Pataphysical Logic)
                        Key Logic

# Key Logic

    □PK0   → PK0         (reflexive key distribution)
    □PK0   → □□PK0       (transitive [metastructure] key distribution)
    □□PK0  → □PK0        (dense key distribution)
    □PK0   → ◊PK0        (serial key distribution)
    PK0    → □◊PK0       (symmetric key distribution; WebApps, CSRF, session, whatever)
    ◊PK0   → □◊          (Euclidean key distribution)
    □(□PK0 → PK0) → □PK0 (well-founded key distribution; centralized hierarchical)
    ◊□PK0  → □◊PK0       (convergent key distrituion)
    ...

Assertion: Solving rebuild times with vector based key distributed node scaling.

Chains are NP hard problems, technically.

# Transformation/Logical Layer to Periodic Table of Information

    Factivity Order        : False information is not stored.
                             (Rollback)
                             √ Persistence

    Motive Order           : Only purposeful information is stored.
                             (Recovery/Checkpointing)
                             √ Uniqueness

    Byzantine Order        : Node having information, i, is a Good Actor.
                             (Fuzzy Voting)
                             √ Scalability

    Incept Assertion Order : Transmission of information, i, implies one has
                             run a checksum or smooth simulation internally.
                             (Rejuvenation)
                             √ Universality

    Assertion Order        : If Node asserts p, then all machines can confirm
                             that Node knows p.
                             (Mask/Material Implication)
                             √ Opacity

    Closure Order          : Node stores all deductions of p assuming Node
                             stores ancestry of p.
                             (Confinement/Reduction)
                             √ No Side-effects

    Anti-luck Order        : Information excludes stochastic luck.
                             (Retry/Retransmission)
                             √ Optional sameness

    Achievement Order      : Information is a technological achievement.
                             (Reconfig)
                             √ Global uniqueness

    Ability Order          : If Node stores p, then the truth of Node’s motive
                             for p must have been ensured through the exercise
                             of Node’s relevant technological abilities.
                             (Diagnose/Adaptive N-version programming)
                             √ Evolvability

    Non-local Order        : Some extension to the Achievement order.
                             (ft-html: <!doctype html><html ◈>)
                             √ Fully Non-dereferenceable


# Physical Layer Design Assumptions of Information

    Distributed        : Interconnected by a high-bandwidth, low-latency network
    High Availability  : Most nodes up most of the time (No one drive failure faults.)
    High Accessibility : Uncorrelated client node failures
    Optimal Sharding   : No files are both: read by many users  and/or  frequently updated by at least one user
    Byzantine          : Small but significant fraction of users will maliciously attempt to destroy or corrupt file data and metadata
    Secure             : Large fraction of users may independently attempt unauthorized accesses
    Provenance         : Each node is  under the control of its immediate user  and  cannot be subverted by other people
    Immutable          : No user sensitive data persist after logout or system reboot
    MTC                : Mathematical Theory of Communications


# Benefits: Fault Tolerance Strategies

    Recovery Blocks
    Self-check / Recursive Pingback (100s)
    Re-express                      (100s, 200s)
    Retry                           (200s, 300s)
    N-versioning                    (semver; 300s, 400s)
    N-copy                          (100s, 300s, 400s)
      * Consistent snapshotting     (e.g. this classic paper from Chandy and Lamport)
    Adaptive N-version programming  (better than semver, distributed logging; 200s, 300s, 400s)
      * Leader election             (e.g. the Bully algorithm)
    Fuzzy Voting                    (200s, 300s)
      * Consensus                   (2-phase commit)
    Reconfig/Rejuvenation           (300s, 400s, 500s)
    Abstraction/Mask                (200s, some 300s, 400s, 500s)
      BQP iff CASE                  (cognitive-tolerance abstraction specification encapsulation)
      * Distributed state machine replication
    Parallel Graph Reduction        (PHT Prediction Modeling with Patricia Triemodel Pruning; 500s)[0]

HTML can handle that, and still be [lightweight](https://www.ampproject.org/).

[0]: Recently, parallel computing is popularly applied to many systems.
Functional programming is suitable for parallel programming because of its
referential transparency and the independency among each program. Unitary
reference architecture or, referential transparency, (supported by CRS)
means that all references to the value are therefore equivalent to the
value itself and the fact that the expression may be referred to from
other parts of the program is of no concern.


# Beyond ‘the Cloud’

![thecloud](images/the_cloud.png)

`programmable data` and `cloud developer`:

    Inductive Metastructure of Code
    On-demand Architecture
    Distributed Resource-oriented Applications
    Molecular Ads (Degradable Media Models)
    Software-defined Machine Identity (Ma2Ma)
    Software-defined Content Identity (Co2Co)

Servers be damned! `smart names` aren’t spam!


# Semiotic Alchemy

    'That is: they deal with issues of significance. Moreover, what computer
    science is, I believe—what history will recognise its 20th-century
    instantiation to have been—is an experimental, synthetic precursor to the
    emergence of this new theory: i.e., something I call semiotic alchemy. Think
    of all those C++ and Java hackers, trying to turn web pages into gold! And
    think, too, of the profusion of rag-tag, untheorised practices, conducted
    in cottages, basements, and garages, that constitute “computing in the wild”:
    they embody a vast wealth of pragmatic and practical understanding—just not
    as-yet very well understood. It all makes such good sense. And it is
    important. Computers are laboratories of middling complexity, between the
    frictionless pucks and inclined planes of mechanics, and the full-blooded
    complexity of the human condition, in terms of which to experiment with,
    and come to understand, the primordial intentional dialectic.'
    — "[God, Approximately](http://www.ageofsignificance.org/people/bcsmith/papers/smith-godapprox4.html)".
    — Brian Cantwell Smith.

# Existing Trends

- They’re already doing it, apparently.

- But maybe these problems are the result of Gettier, and solutions are
  obscured by obscurity (entropy).


# Our Responses

- Positivist Strategies: P2P Government, Anonymous, Occupy, #feelthebern (hashtags), etc.
  - Detached from Technology; Critical of Technology;
  - Self-organizing, Entrenched in the Industrial Economies
- Negative Strategies: Tor, Bittorent and Bitcoin, Dark markets, DDoSing, Wikileaks, etc.
  - Detached from Culture; Transhumanist in Nature
  - Has anyone got an easy cross-platform setup for proxy switching? JavaScript? *shrug*


# Information and Belief

![ptimath](images/discovery-modeling.png)

* 100 : Adaptive N-version programming (Semver all the APIs)
* 200 : Fuzzy Voting
* 300 : Reconfig and Rejuvenation
* 400 : Abstraction (Byzantine Fault Tolerance with Abstract Specification Encapsulation)
* 500 : Parallel Graph Reduction


# Prospects

- MTC
- Gettier
- Logic Layer

<div style='max-width: 100%'>
<img alt='ptisource' src='table-source.png' />
</div>


# Philosophical Problems

- Dual-aspect Zero Knowledge Systems
- NoDef Minimalistic Framework of Information


# Models: Existing Ideas

<img alt="hfactor" src="images/Blocknext-ThinkingEngine-Internal.png" />

* Embodied Cognition
* Cogito
* “AI”


# Models: New Hotness on “Blockchains”

<img alt="blockchain" src="images/Blocknext-ExtendedEngine-Internal.png" />

- p > q (Gambler’s Ruin Solution; Bitcoin solution)
- r > g (return of return to wealth versus economic growth rate)
- Any relation? (Pbbt.) What kind of statement is it?
- Conjecture, Prediction, Principle?
- Is Piketty’s Tax justified? Is Satoshi’s? For all Bad Actors?

Response: There’s no proof that high debt causes low wealth.
Response: Redistribution of wealth engenders dependency, predistribution empowers.
Response: Piketty is stating that society has made `r > g` a *good prediction*.
Response: Bitcoin is only a *mathematical statement of the problem, not a solution*.

# Computationally Weak Forces

Computationally bound identity systems are hazardous and tend toward
maximizing r > g. We must correct for this flaw of infocological artifacts.

# Synthesis of Ideas: Dual-aspect Integration

<img alt="dto" src="images/Blocknext-DeflationaryOrdersTruthEngine-Internal.png" />


# Practical Infocology

Popper-Quine-Mill-Weisberg (Type-Q Materialism): “to be is to be a value of a bound variable”

<img alt="pti" src="images/Blocknext-Architecture-Internal.png" />


# Metamedia with Intent

<div style='max-width: 100%'>
<img alt='iam' src='images/BlockNext-AssetManagement.png' />
</div>

Metamedia with Intent


# Metamedia with Intent

<img alt="dmm" src="images/FileType-SecuritySentiment.png" />

- Degradable Media Models (ARIA codes, metacodes, isotype languages for all filetypes): http://lcamtuf.coredump.cx/squirrel/
- Latent Accessibility of all Formats: https://en.wikipedia.org/wiki/List_of_file_formats
- Bu2Bu, Cu2Cu, Ma2Ma (machine-to-machine business), Co2Co (content-to-content business)


# Methodological Strategies

- Datalogical/Architectural Mass: Periodic Table of Information; new component to Web Science: infocology, infosthetics,
- Syntactic Entropy Modeling: PTI+CRS
- Pragmatics of Infosys: Organelles, Cells, Molecular Designs, etc.

A way to build apps (drapps) today: Distributed [Resource-oriented](http://roca-style.org/) Applications

- No Bytecode
- Metacodes expressed in [posh](http://microformats.org/wiki/posh).
- Quantizable State Machine
- Keyless (NoAPIKey) Identity
