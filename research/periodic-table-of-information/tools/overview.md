 # Overview

## Print Interactive

    $ pandoc -t beamer --self-contained -s index.md -o index.tex

Or:

    $ pandoc -V geometry:margin=1in --latex-engine=xelatex -o output.pdf index.md

## Interactive

To produce the slide show, simply type

    $ pandoc -t slidy --self-contained -s index.md -o index.html

Or `-t`:

    slideous
    dzslides
    beamer
