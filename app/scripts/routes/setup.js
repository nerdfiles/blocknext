/**(
 * @fileOverview ./app/scripts/routes/setup.js
 * @description
 * Setup routes contract which designates thinghood, essentially.
 * )
 */
define([], function () {
  "use strict";

  return function Setup ($routeProvider, $locationProvider) {

    $locationProvider.html5Mode(true);

    $routeProvider

      // Diagnostic Routes, entailing a Canonical Sitemap Specification (CSS). Thanks, CRS.
      .when('/201',
        {
        controllerUrl : 'self/statuses/201/base',
        templateUrl   : 'scripts/statuses/201/base.html'
        })

      .when('/202',
        {
        controllerUrl : 'self/statuses/202/base',
        templateUrl   : 'scripts/statuses/202/base.html'
        })

      .when('/203',
        {
        controllerUrl : 'self/statuses/203/base',
        templateUrl   : 'scripts/statuses/203/base.html'
        })

      .when('/204',
        {
        controllerUrl : 'self/statuses/204/base',
        templateUrl   : 'scripts/statuses/204/base.html'
        })

      .when('/205',
        {
        controllerUrl : 'self/statuses/205/base',
        templateUrl   : 'scripts/statuses/205/base.html'
        })

      // Open Routes
      .when('/',
        {
        controllerUrl : 'scripts/modules/home/base',
        templateUrl   : 'views/pages/home.html'
        })

      // Privileged Routes
      // Implicates UpdateAction space by default; so these need to be set up,
      // and HTTP self/statuseses as well for PTI and CRS to be integrated.
      .whenAuthenticated('/account', {
        templateUrl   : 'views/pages/account.html',
        controllerUrl : 'scripts/modules/account/base'
      });
  }

});
