/**(
 * @fileOverview ./app/scripts/routes/secured.js
 * @description
 * Secured routes config.
 * )
 */
define([], function () {

  return function Secured (__interface__) {
    /**
     * @constructor
     * @import env
     */
    __interface__.constant('SECURED_ROUTES', {})
    __interface__.constant('FBURL', 'https://blocknext.firebaseio.com')
    __interface__.constant('SIMPLE_LOGIN_PROVIDERS', ['facebook'])
    __interface__.constant('loginRedirectPath', '/login')

  };
});

