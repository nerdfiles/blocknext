require.config({

  "baseUrl": "./scripts",

  "paths": {

    // application core
    "interface": "interface",

    // routes
    "routes/setup"        : "routes/setup",
    "routes/authenticate" : "routes/authenticate",
    "routes/secured"      : "routes/secured",
    "routes/initialize"   : "routes/initialize",

    // environment configurations
    "environment/database"   : "environment/database",
    "environment/initialize" : "environment/initialize",
    "environment/analytics"  : "environment/analytics",

    // module controllers
    "modules/main"    : "modules/main",
    "modules/chat"    : "modules/chat",
    "modules/login"   : "modules/login",
    "modules/account" : "modules/account",
    "modules/home"    : "modules/home",

    // data services
    "services/geocoder" : "services/geocoder",
    "services/amort"    : "services/amort",

    // components
    "auth/ngShowAuth"   : "components/auth/ngShowAuth",
    "auth/ngHideAuth"   : "components/auth/ngHideAuth",
    "self/ngCall"       : "components/self/ng__call__",
    "version/ngDisplay" : "components/version/ngDisplay",
    "noJs/ngNoJs"       : "components/noJs/ngNoJs",

    // filters, denomination conversions, text abbreviations, typesetting issues
    "filters/reverse" : "filters/reverse",

    // angular
    "angular"           : "ext/angular.min",
    "angular-waypoints" : "ext/angular-waypoints.min",
    "angular-material"  : "ext/angular-material.min",
    "angular-route"     : "ext/angular-route.min",
    "angularAMD"        : "ext/angularAMD",
    "ngload"            : "ext/ngload",
    "angular-animate"   : "ext/angular-animate.min",
    "angular-cookies"   : "ext/angular-cookies.min",
    "angular-resource"  : "ext/angular-resource.min",
    "angular-sanitize"  : "ext/angular-sanitize.min",
    "angular-touch"     : "ext/angular-touch.min",
    "angular-aria"      : "ext/angular-aria.min",
    "angular-messages"  : "ext/angular-messages.min",
    "angular-storage"   : "ext/ngStorage",
    "angular-segement"  : "ext/segement.min",
    "angularfire"       : "ext/angularfire.min",
    "firebase"          : "ext/firebase",
    "lodash"            : "ext/lodash.min",
    "segment"           : "ext/segment.min",

    // leaflet
    "leaflet"                   : "ext/leaflet",
    "osm"                       : "ext/OSMBuildings-Leaflet",
    "angular-leaflet-directive" : "ext/angular-leaflet-directive.min",
    "esri-leaflet-geocoder"     : "ext/esri-leaflet-geocoder",
    "esri-leaflet"              : "ext/esri-leaflet",

    // d3
    "d3" : "ext/d3.min",

    // jquery
    "jquery"        : "ext/jquery-1.9.1.min",
    "jquery.unveil" : "ext/jquery.unveil.min"
  },

  "shim": {
    // analytics
    "segment": ['angular'],

    // jquery
    "jquery.unveil": [
      'jquery'
    ],

    // utils
    "lodash": {
      "exports": "_"
    },

    // angular
    "angular": {
      "exports": "angular"
    },
    "angular-waypoints": [
      "angular"
    ],
    "angular-material": [
      'angular'
    ],
    "angular-route": [
      "angular"
    ],
    "angularAMD": [
      "angular"
    ],
    "ngload": [
      "angularAMD"
    ],
    "angular-aria": [
      "angular"
    ],
    "angular-messages": [
      "angular"
    ],
    "angular-animate": [
      "angular"
    ],
    "angular-cookies": [
      "angular"
    ],
    "angular-resource": [
      "angular"
    ],
    "angular-sanitize": [
      "angular"
    ],
    "angular-touch": [
      "angular"
    ],
    "angularfire": [
      "angular",
      "firebase"
    ],
    "angular-leaflet-directive": [
      'angular',
      'leaflet'
    ],

    // leaflet
    "leaflet": {
      "deps": ["angular"],
      "exports": "L"
    },
    "osm": ['angular-leaflet-directive'],
    "esri-leaflet": ['angular-leaflet-directive'],
    "esri-leaflet-geocoder": ["esri-leaflet"]
  },

  "deps": ['interface']
});
