/**(
 * @fileOverview ./app/scripts/components/self/ng__call__.js
 * @description
 * A kind of declarative implement of a callback implementation indicative of
 * most control languages.
 *
 * Imagine that we extend the method of asynchronous module definition
 * through-goingly? Certain programmation insertions into the DOM will thereby
 * call statues. This way developers can then try out Inspector Debugger tool
 * DOM insertions and get the expected GET request as if they were to write
 * the API themselves.
 * @usage
 * <div
 *   ng-call
 *   status="202"
 *   rel="(payment) sequential--retry"
 * />
 * )
 */

define([
  'interface'
], function GET__ng__call__ (__interface__) {
  // @apiDoc GET
  // @apiDescription
  // For when we want our notification to call any status, for instance.

  "use strict";

  function ng__call__ ($http) {
    // @ngdoc directive

    function link ($scope, element, attrs) {
      // @inner
      var prefixUrl = 'scripts/statuses/'
      var endfixUrl = '/partials/base.html'

      $scope.$watch('status', function (newVal oldVal) {
        if (newVal) {
          $http.get(prefixUrl + newVal, function (data) {
            console.dir(data);
          });
        }
      });
    }

    return {
      restrict : 'A',
      scope    : {
        status : '@',
        rel    : '@'
      },
      link     : link
    };
  }

  __interface__.directive('ngCall', [
    '$http',
    ng__call__
  ]);

  return ng__call__;
});
