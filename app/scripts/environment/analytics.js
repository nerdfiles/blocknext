/**(
 * @fileOverview ./app/scripts/environment/analytics.js
 * @description
 * Segement.io environment configuration.
 * )
 */
define([], function () {

  return function Analytics (__interface__) {

    __interface__.constant('GlobalEvents', {
    });

    __interface__.constant('segmentConfig', {
      apiKey: 'ABDUo5Zkv4Hid6BfIZHjJ4LeHCAnNSnT'
    });

    __interface__.config(function (segmentProvider, GlobalEvents) {
      segmentProvider.setEvents(GlobalEvents);
      //segmentProvider.track('test');
    });

    __interface__.config(['$compileProvider', function ($compileProvider) {
      $compileProvider.debugInfoEnabled(false);
    }]);

  }
});
