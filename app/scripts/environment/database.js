/**(
 * @fileOverview ./app/scripts/environment/database.js
 * )
 */

define([], function () {
  "use strict";

  return function Database (__interface__) {

    __interface__
      .factory('Ref', ['$window', 'FBURL', function($window, FBURL) {
        return new $window.Firebase(FBURL);
      }])
      .factory('Auth', function ($firebaseAuth, Ref) {
        return $firebaseAuth(Ref);
      });

  };

});
