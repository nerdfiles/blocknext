'use strict';

define([
  "angularAMD",
  "leaflet",
  "osm",

  "routes/setup",
  "routes/secured",
  "routes/authenticate",

  "environment/analytics",
  "environment/initialize",
  "environment/database",

  "angular-route",
  "angular-animate",
  "angular-aria",
  "angular-messages",
  "angular-cookies",
  "angular-resource",
  "angular-sanitize",
  "angular-storage",
  "angularfire",
  "segment",
  "angular-material",

  "auth/ngShowAuth",
  "auth/ngHideAuth",
  //"self/ngCall",
  "version/ngDisplay",
  "noJs/ngNoJs",

  //"filters/reverse"
  //"angular-waypoints"
], function (angularAMD, leaflet, OSMBuildings, setup, secured, authenticate, analytics, initialize, database) {

  //window.OSMBuildings = OSMBuildings;

  var applicationDependencies = [
    'firebase',
    "ngRoute",
    "ngAnimate",
    "ngCookies",
    "ngResource",
    "ngSanitize",
    'ngAria',
    'ngMessages',
    'leaflet-directive',
    'ngStorage',
    'ngMaterial',
    "ngSegment"
    //'zumba.angular-waypoints'
  ];

  /**
   * @ngdoc overview
   * @name blocknext
   * @description
   * # blocknext.com Web application.
   *
   * Main modules of the application.
   */
  var __interface__ = angular.module("blocknext", applicationDependencies);

  // Front End Middleware.
  secured(__interface__);
  analytics(__interface__);
  database(__interface__);

  __interface__

    .config([
      '$routeProvider',
      'SECURED_ROUTES',
     authenticate
    ])

    .config([
      '$routeProvider',
      '$locationProvider',
      setup
    ])

    .run([
      '$rootScope',
      '$location',
      'Auth',
      'SECURED_ROUTES',
      'loginRedirectPath',
      '$window',
      initialize
    ]);

 __interface__.directive('unveil', function () {
    return {
      link: function (scope, element, attrs) {
          $(element).unveil();
      }
    };
  });

  return angularAMD.bootstrap(__interface__);

});
